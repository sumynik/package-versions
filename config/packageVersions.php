<?php

return [
    /**
     * Токен доступу по якому буде дозволятися доступ до пакета
     */
    'key' => env('SUMYNIK_PACKAGE_VERSIONS_KEY', null),

    /**
     * Масив IP адрес з яких буде дозволено доступ до пакета
     */
    'ips' => env('SUMYNIK_PACKAGE_VERSIONS_IPS', '["127.0.0.1"]'),

    /**
     * При значенні true буде віддавати весь список встановлених пакетів
     */
    'full_package' => env('SUMYNIK_PACKEGE_VERSIONS_FULL', false),

    /**
     * Особисті токени доступу до GITLAB
     */
    'gitlab_key' => env('SUMYNIK_PACKAGE_VERSIONS_GITLAB_KEY', null),
];
