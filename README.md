## Встановлення

1. В **composer.json** додаємо
```json
"repositories": [
    {
      "type": "vcs",
      "url":  "git@gitlab.com:sumynik/package-versions.git"
    }
],
```
2. В терміналі виконати команду
```
composer require sumynik/package-versions
```

3. В файлі .env додати
    - SUMYNIK_PACKAGE_VERSIONS_KEY= - токен доступу
    - SUMYNIK_PACKAGE_VERSIONS_IPS=["127.0.0.1"] - масив IP адрес з яких дозволено доступ в форматі JSON

4. Додати в файлі **config/database.php** в розділ **'connections'**
  ```php
      'package_versions_sqlite' => [
          'driver' => 'sqlite',
          'url' => env('SUMYNIK_PACKAGE_VERSIONS_DATABASE_URL'),
          'database' => env('SUMYNIK_PACKAGE_VERSIONS_DB_DATABASE', database_path('sumynik_modulus.sqlite')),
          'prefix' => '',
          'foreign_key_constraints' => env('SUMYNIK_PACKAGE_VERSIONS_DB_FOREIGN_KEYS', true),
      ],
  ```
5. Виконати в терміналі **php artisan vendor:publish --tag=package-version-db**
6. Виконати в терміналі **php artisan migrate**
7. Додати в **app\Console\Kernel.php** в метод **schedule** (Має бути налаштовано [Запуск планувальника](https://laravel.com/docs/master/scheduling#running-the-scheduler) )
```php
$schedule->command('sumynik-package-version:parse-versions')->daily();
```
8. Виконати команду **php artisan sumynik-package-version:init-version**
9. Виконати команду **php artisan sumynik-package-version:parse-versions**
10. Для приватних GIT репозитаріїв додати токен доступу

## Налаштування
- SUMYNIK_PACKAGE_VERSIONS_KEY - токен доступу по якому буде дозволятися доступ до пакета
- SUMYNIK_PACKAGE_VERSIONS_IPS - масив IP адрес з яких буде дозволено доступ до пакета
- SUMYNIK_PACKEGE_VERSIONS_FULL - при значенні **true** буде обробляти весь список встановлених пакетів
- SUMYNIK_PACKAGE_VERSIONS_GITLAB_KEY - Особисті токени доступу до GITLAB [Токени доступу](https://gitlab.com/-/user_settings/personal_access_tokens)
- В **$schedule->command('sumynik-package-version:parse-versions')->daily();** можна змінювати **daily()** відповідно до [документації laravel](https://laravel.com/docs/master/scheduling#schedule-frequency-options)

## Використання
Команди в терміналі:
- php artisan sumynik-package-version:init-version - Заповнює DB списком пакетів
- php artisan sumynik-package-version:parse-versions - Перевіряє нові версії пакетів

Для доступу в API в хедері треба передати заголовок **Authorization** зі значенням **Bearer <значення SUMYNIK_PACKAGE_VERSIONS_KEY>**

- GET /api/composer/versions - Повертає список пакетів, в форматі json, де **name** - ім'я пакету, **type** - тип пакета(main головний, dep залежність) **description** - опис пакету, **version** - поточна версія, **new_version** - версія до якої можна оновитися

