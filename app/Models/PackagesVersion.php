<?php

namespace Sumynik\PackageVersions\App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Sumynik\PackageVersions\App\Models\PackagesVersion
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $type
 * @property string $name
 * @property string $constraint
 * @property string $description
 * @property string $version
 * @property string $new_version
 * @method static \Illuminate\Database\Eloquent\Builder|PackagesVersion newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PackagesVersion newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PackagesVersion query()
 * @method static \Illuminate\Database\Eloquent\Builder|PackagesVersion whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PackagesVersion whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PackagesVersion whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PackagesVersion whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PackagesVersion whereConstraint($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PackagesVersion whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PackagesVersion whereVersion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PackagesVersion whereNewVersion($value)
 * @mixin \Eloquent
 */
class PackagesVersion extends Model
{
    use HasFactory;

    /**
     * Головна пакет
     */
    const TYPE_MAIN = "main";

    /**
     * Пакет залежності
     */
    const TYPE_DEP = "dep";

    /**
     * The database connection that should be used by the model.
     *
     * @var string
     */
    protected $connection = 'package_versions_sqlite';

    protected $fillable = [
        "created_at",
        "updated_at",
        "type",
        "name",
        "constraint",
        "description",
        "version",
        "new_version"
    ];
}
