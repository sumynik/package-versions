<?php

namespace Sumynik\PackageVersions\App\Actions;

use Sumynik\PackageVersions\App\Models\PackagesVersion;

class InitVersionsAction
{
    public function __invoke(): void
    {
        $this->parseComposerJson();
        $this->parseComposerLock();
    }

    /**
     * Обробляє пакети з composer.json
     * @return void
     */
    private function parseComposerJson(): void
    {
        $composerJson = json_decode(file_get_contents("." . DIRECTORY_SEPARATOR . "composer.json"));
        $packagesVersions = $this->packagesVersions();

        $packagesProd = $packagesVersions->filter(function (PackagesVersion $item, int $key) {
            return $item->type == PackagesVersion::TYPE_MAIN;
        })->map(function (PackagesVersion $item) {
            return $item->name;
        });

        foreach ($composerJson->require as $packageProd=>$versionProd)
        {
            if(str_contains($packageProd, "/") && !in_array( $packageProd, $packagesProd->all())){
                PackagesVersion::create([
                    "type" => PackagesVersion::TYPE_MAIN,
                    "name" => $packageProd,
                    "constraint" => $versionProd,
                    "description" => "",
                    "version" => "",
                    "new_version" => ""
                ]);
            }
        }
    }

    /**
     * Обробляє пакети з composer.lock
     * @return void
     */
    private function parseComposerLock(): void
    {
        $packagesVersions = $this->packagesVersions()->map(function (PackagesVersion $item) {
            return $item->name;
        })->all();
        $composerLock = json_decode(file_get_contents("." . DIRECTORY_SEPARATOR . "composer.lock"));
        $packagesCreate = $packagesUpdate = [];

        foreach ($composerLock->packages as $packageLock){
            if(in_array( $packageLock->name, $packagesVersions)){
                $packagesUpdate[$packageLock->name] = [
                    "description" => ($packageLock->description ?? ""),
                    /*"version" => $packageLock->version,
                    "new_version" => $packageLock->version*/
                ];
                if(config('packageVersions.full_package') && isset($packageLock->require)){
                    $packagesCreate = $this->parseRequire($packageLock->require,$packagesVersions,$packagesCreate);
                }
            } elseif (config('packageVersions.full_package') && !in_array( $packageLock->name, $packagesVersions)){
                $packagesCreate[$packageLock->name]["description"] = ($packageLock->description ?? "");
                $packagesCreate[$packageLock->name]["version"] = "";//$packageLock->version;
                $packagesCreate[$packageLock->name]["new_version"] = "";//$packageLock->version;

                if(isset($packageLock->require)){
                    $packagesCreate = $this->parseRequire($packageLock->require,$packagesVersions,$packagesCreate);
                }
            }
        }

        if(!empty($packagesCreate)){
            foreach ($packagesCreate as $packageCreate){
                if(isset($packageCreate["description"])){
                    PackagesVersion::create($packageCreate);
                }
            }
        }

        if(!empty($packagesUpdate)){
            foreach ($packagesUpdate as $packageName=>$packageData){
                PackagesVersion::whereName($packageName)->first()->update($packageData);
            }
        }
    }

    /**
     * Обробляє залежності пакета
     * @param $require
     * @param $packagesVersions
     * @param $packagesCreate
     * @return array
     */
    private function parseRequire($require, $packagesVersions, $packagesCreate): array
    {
        foreach ($require as $requireName=>$requireVersion){
            if(str_contains($requireName, "/") && !in_array( $requireName, $packagesVersions)){
                $packagesCreate[$requireName]["type"] = PackagesVersion::TYPE_DEP;
                $packagesCreate[$requireName]["name"] = $requireName;
                $packagesCreate[$requireName]["constraint"] = $requireVersion;
            }
        }
        return $packagesCreate;
    }

    /**
     * @return PackagesVersion[]
     */
    private function packagesVersions()
    {
        return PackagesVersion::get();
    }
}
