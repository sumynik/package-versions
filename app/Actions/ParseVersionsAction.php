<?php

namespace Sumynik\PackageVersions\App\Actions;

use Sumynik\PackageVersions\App\Models\PackagesVersion;
use Illuminate\Support\Facades\Http;

class ParseVersionsAction
{
    public function __invoke(): void
    {
        $composerLock = json_decode(file_get_contents("." . DIRECTORY_SEPARATOR . "composer.lock"));
        $packagesVersions = PackagesVersion::get();
        $packagesVersionsArray = $packagesVersions->map(function (PackagesVersion $item) {
            return $item->name;
        })->all();

        foreach ($composerLock->packages as $package){
            if(in_array($package->name, $packagesVersionsArray)){
                $result = false;
                $packagesVersion = $packagesVersions->first(function (PackagesVersion $value, int $key) use ($package) {
                    return $value->name == $package->name;
                });

                $response = Http::get("https://repo.packagist.org/p2/{$package->name}.json");
                if($response->ok()) {
                    $version = $versionNew = "0.0.0.0";
                    $releases = $response->json("packages", []);

                    if(!empty($releases[$package->name])){
                        foreach ($releases[$package->name] as $release){
                            /**
                             * Знаходимо номер встановленої версії
                             */
                            if(isset($release['dist']) && $package->dist->reference == $release['dist']['reference']){
                                $version = $release['version_normalized'];
                            } elseif(isset($release['source']) && $package->dist->reference == $release['source']['reference']){
                                $version = $release['version_normalized'];
                            }
                        }
                        $versionNew = $version;
                        foreach ($releases[$package->name] as $release){
                            $versionNew = $this->testVersion($packagesVersion, $release['version_normalized'], $versionNew);
                        }
                    }

                    $result = PackagesVersion::whereName($package->name)->first()->update([
                        "version" => $version,
                        "new_version" => $versionNew
                    ]);
                }

                if (!$result) {
                    $packageName = urlencode($package->name);
                    $response = Http::get("https://gitlab.com/api/v4/projects/{$packageName}/repository/tags");

                    if($response->ok()) {
                        $result = $this->processingGitData($response, $package, $packagesVersion);
                    }
                }

                if (!$result && config('packageVersions.gitlab_key')) {
                    $packageName = urlencode($package->name);
                    $response = Http::withHeaders([
                        'Authorization' => 'Bearer ' . config('packageVersions.gitlab_key')
                    ])->get("https://gitlab.com/api/v4/projects/{$packageName}/repository/tags");
                    if($response->ok()) {
                        $result = $this->processingGitData($response, $package, $packagesVersion);
                    }
                }
            }
        }
    }

    /**
     * Обробляє відповідь на запит до GIT
     *
     * @param $response
     * @param $package
     * @param $packagesVersion
     * @return bool|int
     */
    private function processingGitData($response, $package, $packagesVersion): bool|int
    {
        $version = "0.0.0.0";
        $releases = $response->json();

        foreach ($releases as $release){
            /**
             * Знаходимо номер встановленої версії
             */
            if($package->dist->reference == $release['target']
                || $package->dist->reference == $release['commit']['id']){
                $version = $release['name'];
            }
        }

        $versionNew = $version;
        foreach ($releases as $release){
            $versionNew = $this->testVersion($packagesVersion, $release['name'], $versionNew);
        }
        return PackagesVersion::whereName($package->name)->first()->update([
            "version" => $version,
            "new_version" => $versionNew
        ]);
    }

    /**
     * Отримуємо номер останньої доступної версії для оновлення
     *
     * @param PackagesVersion $packagesVersion
     * @param string $versionNormalized
     * @param string $versionNew
     * @return string|bool
     */
    private function testVersion(PackagesVersion $packagesVersion, string $versionNormalized, string $versionNew): string|bool
    {

        if(str_contains($packagesVersion->constraint, "||")) {
            $constraints = explode("||", $packagesVersion->constraint);

            foreach ($constraints as $constraint) {
                $versionNew = $this->comparisonVersion(trim($constraint), $versionNormalized, $versionNew);
            }
        }elseif(str_contains($packagesVersion->constraint, "|")){
            $constraints = explode("|", $packagesVersion->constraint);

            foreach ($constraints as $constraint){
                $versionNew = $this->comparisonVersion(trim($constraint), $versionNormalized, $versionNew);
            }
        } else {
            $versionNew = $this->comparisonVersion($packagesVersion->constraint, $versionNormalized, $versionNew);
        }

        return $versionNew;
    }

    /**
     * Виявлення останньої доступної версії пакета
     * @param string $constraint Вимого до версії
     * @param string $versionNormalized Нова версія попередньої ітерації
     * @param string $versionNew Версія отримана з репозиторію пакетів
     * @return string|bool
     */
    private function comparisonVersion(string $constraint, string $versionNormalized, string $versionNew): string|bool
    {
        if(!str_contains(strtolower($versionNormalized), "beta") && !str_contains(strtolower($versionNormalized), "rc")) {
            switch (substr($constraint, 0, 1)) {
                case "^":
                    $comparison = $this->comparisonNodsVersion($constraint, $versionNormalized, $versionNew);

                    if ($comparison !== false) {
                        $versionNew = $comparison;
                    }
                    break;
                case "*":
                    $comparison = $this->comparisonNodsVersion($constraint, $versionNormalized, $versionNew, true);
                    if ($comparison !== false) {
                        $versionNew = $comparison;
                    }
                    break;
            }
        }

        return $versionNew;
    }

    /**
     * @param string $constraint Вимого до версії
     * @param string $versionNormalized Нова версія попередньої ітерації
     * @param string $versionNew Версія отримана з репозиторію пакетів
     *
     * @return string | boolean
     */
    private function comparisonNodsVersion(string $constraint, string $versionNormalized, string $versionNew, $isMaxVersion = false)
    {
        $versionNewTemp = false;
        /**
         * Розбиваємо вимоги та версії на частини
         */
        // Вимоги до версії розбито на частини
        $constraintExplode = explode(".", substr($constraint, 1));
        // Версія отримана з репозиторію
        $releaseExplode = explode(".", $versionNormalized);
        // Визначена нова версія попередньою ітерацією
        $versionNewExplode = explode(".", $versionNew);

        if($constraintExplode[0] == $releaseExplode[0] && $constraintExplode[0] == $versionNewExplode[0]) {
            $versionNewTemp = $releaseExplode[0];
        } elseif ($isMaxVersion && $releaseExplode[0] >= $versionNewExplode[0]){
            $versionNewTemp = $releaseExplode[0];
        }

        if($versionNewTemp !== false) {
            $comparison_1 = $this->comparisonNodVersion(
                1,
                $constraintExplode,
                $versionNewExplode,
                $releaseExplode
            );

            if ($comparison_1 === false) {
                $versionNewTemp = false;
            } else {
                $versionNewTemp .= "." . $comparison_1;
            }
        }

        if($versionNewTemp !== false) {
            $comparison_2 = $this->comparisonNodVersion(
                2,
                $constraintExplode,
                $versionNewExplode,
                $releaseExplode
            );

            if ($comparison_2 === false) {
                $versionNewTemp = false;
            } else {
                $versionNewTemp .= "." . $comparison_2;
            }
        }

        if($versionNewTemp) {
            $comparison_3 = $this->comparisonNodVersion(
                3,
                $constraintExplode,
                $versionNewExplode,
                $releaseExplode
            );

            if ($comparison_3 === false) {
                $versionNewTemp = false;
            } else {
                $versionNewTemp .= "." . $comparison_3;
            }
        }

        return $versionNewTemp;
    }

    /**
     * @param integer $key Номер скриньки в масиві
     * @param array $constraint Вимоги до версії розбито
     * @param array $versionNew Версія з попередньою ітерацією
     * @param array $release Версія отримана з репозиторію
     * @return false|mixed
     */
    private function comparisonNodVersion(int $key, array $constraint, array $versionNew, array $release): mixed
    {
        $version = false;
        if(isset($constraint[$key]) && $constraint[$key] <= $release[$key]
            && $versionNew[$key] <= $release[$key]){
            $version = $release[$key];
        } elseif(!isset($constraint[$key]) && $versionNew[$key] <= $release[$key]){
            $version = $release[$key];
        }

        return $version;
    }
}
