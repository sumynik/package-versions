<?php

namespace Sumynik\PackageVersions\App\Actions;

use Sumynik\PackageVersions\App\Models\PackagesVersion;

class GetComposerVersionsAction
{
    public function __invoke()
    {
        return PackagesVersion::select(['name', 'type', 'description', 'version', 'new_version'])->get();
    }
}
