<?php

namespace Sumynik\PackageVersions\App\Console\Commands;

use Illuminate\Console\Command;
use Sumynik\PackageVersions\App\Actions\InitVersionsAction;

class InitVersionsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sumynik-package-version:init-version';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Заповнює DB списком пакетів';

    /**
     * Execute the console command.
     */
    public function handle(InitVersionsAction $action)
    {
        $action();
    }
}
