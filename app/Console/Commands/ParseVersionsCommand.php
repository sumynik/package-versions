<?php

namespace Sumynik\PackageVersions\App\Console\Commands;

use Illuminate\Console\Command;
use Sumynik\PackageVersions\App\Actions\ParseVersionsAction;

class ParseVersionsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sumynik-package-version:parse-versions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Розбір версій пакетів composer';

    /**
     * Execute the console command.
     */
    public function handle(ParseVersionsAction $action)
    {
        $action();
    }
}
