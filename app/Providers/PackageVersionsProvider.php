<?php

namespace Sumynik\PackageVersions\App\Providers;
use Sumynik\PackageVersions\App\Console\Commands\InitVersionsCommand;
use Sumynik\PackageVersions\App\Console\Commands\ParseVersionsCommand;

class PackageVersionsProvider extends \Illuminate\Support\ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->mergeConfigFrom(
            __DIR__.'/../../config/packageVersions.php', 'packageVersions'
        );
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        $this->loadRoutesFrom(__DIR__ . '/../../routes/api.php');
        $this->loadMigrationsFrom(__DIR__.'/../../database/migrations');

        if ($this->app->runningInConsole()) {
            $this->commands([
                ParseVersionsCommand::class,
                InitVersionsCommand::class
            ]);
        }

        $this->publishes([
            __DIR__.'/../../database/db' => database_path('/')
        ], 'package-version-db');
    }
}
