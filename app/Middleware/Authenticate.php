<?php

namespace Sumynik\PackageVersions\App\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class Authenticate
{
    public function handle(Request $request, Closure $next)
    {
        $key = config('packageVersions.key');
        $ips = json_decode(config('packageVersions.ips'));
        $headers = trim($request->header('Authorization'));
        // HEADER: Get the access token from the header
        if (!empty($headers) && $key) {
            if (preg_match('/Bearer\s(\S+)/', $headers, $matches)
                && in_array($request->ip(), $ips)) {
                if($matches[1] === $key){
                    return $next($request);
                }
            }
        }

        return response(["message"=> "Unauthenticated."]);
    }
}
