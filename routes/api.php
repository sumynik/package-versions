<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Sumynik\PackageVersions\App\Actions\GetComposerVersionsAction;
use Sumynik\PackageVersions\App\Middleware\Authenticate;

Route::middleware(['api', Authenticate::class])
    ->prefix('api')
    ->group(function (){
        /**
         * Віддає список пакетів, версії та останню доступну версію
         */
        Route::get('/composer/versions', function (Request $request, GetComposerVersionsAction $action) {
            return $action();
        });
    });
