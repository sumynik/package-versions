<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::connection('package_versions_sqlite')->create('packages_versions', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->enum('type', ['main', 'dep']);
            $table->string('name');
            $table->string('constraint');
            $table->text('description');
            $table->string('version');
            $table->string('new_version');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::connection('package_versions_sqlite')->dropIfExists('packages_versions');
    }
};
